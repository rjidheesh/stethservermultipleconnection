/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stethserver;

import java.io.IOException;
import java.io.InputStream;
import static java.lang.System.exit;
import java.net.ServerSocket;
import java.net.Socket;

/**
 *
 * @author Jideesh
 */
public class StethServer {

    public static  ServerSocket socket;
    public static Socket connectionsocket;
    public static Socket[] ESockets = new Socket[10];
    public static Socket[] CSockets = new Socket[10];
    public static boolean status[][] = new boolean[10][2];
    
    
    public static void main(String[] args) throws IOException 
    {
        try 
        {
            socket = new ServerSocket(1235);
        } catch (IOException ex) {
            System.out.println("Cant open... Port is used by another program");
            exit(0);
        }
        
        CThread patient[] = new CThread[10];
        EThread doctor[] = new EThread[10];
        
        
        for(int i=0;i<10;i++)
        {
            status[i][1] = false;
            status[i][0] = false;
        }
        
            byte [] temp = new byte[2];
           
        while(true)
        {
            System.out.println("Waiting for connection");
            connectionsocket = socket.accept();
            InputStream is = connectionsocket.getInputStream();
//     
            System.out.println("connection accepted");
            
            while(is.read(temp)>0 )
            {
//                System.out.print("e");
                if(temp[0]=='C' && temp[1]=='0')
                {
                    for(int i=0;i<10;i++)
                    {
                        if(!status[i][0])
                        {
                            CSockets[i]=connectionsocket;
                            patient[i] = new CThread();
                            patient[i].threadnum=i;
                            patient[i].is = is;
                            status[i][0]=true;
                            patient[i].start();
                            break;
                        }
                        else if(i==9)
                        {
                            System.out.println("No avalable threads");
                            byte[] temporary = new byte [3];
                            temporary[0]='E';temporary[0]='R';temporary[0]='R';
                            connectionsocket.getOutputStream().write(temporary);
                            connectionsocket.close();
                        }
                    }
                    break;
                }
                else if(temp[0]=='E' && temp[1]=='0')
                {
                    for(int i=0;i<10;i++)
                    {
                        if(!status[i][1])
                        {
                            ESockets[i]=connectionsocket;
                            System.out.println(i);
                            doctor[i] = new EThread();
                            doctor[i].threadnum=i;
                            doctor[i].is= is;
                            status[i][1]=true;
                            doctor[i].start();
                            break;
                        }
                        else if(i==9)
                        {
                            System.out.println("No avalable threads");
                            byte[] temporary = new byte [3];
                            temporary[0]='E';temporary[0]='R';temporary[0]='R';
                            connectionsocket.getOutputStream().write(temporary);
                            connectionsocket.close();
                        }
                    }
                    break;
                }
                else if(temp[0]=='E')
                {
                    char ch = (char)temp[1];
                    int s= Character.getNumericValue(ch);
//                    System.out.println(s);
                    ESockets[s]=connectionsocket;
                    System.out.println(s);
                    doctor[s] = new EThread();
                    doctor[s].threadnum=s;
                    doctor[s].is= is;
                    status[s][1]=true;
                    doctor[s].start();
                    break;
                }
                else if(temp[0]=='C')
                {
                    char ch = (char)temp[1];
                    int s= Character.getNumericValue(ch);
//                    System.out.println(s);
                    CSockets[s]=connectionsocket;
                    System.out.println(s);
                    patient[s] = new CThread();
                    patient[s].threadnum=s;
                    patient[s].is= is;
                    status[s][0]=true;
                    patient[s].start();
                    break;
                }
            }
        }
    }      
}